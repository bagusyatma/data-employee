import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.sql.SQLException;

public class ServerEmployee extends WebSocketServer {
    public ServerEmployee(int port){
        super(new InetSocketAddress(port));
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        int port = 7070;
        try {
            port = Integer.parseInt( args[ 0 ] );
        } catch ( Exception ex ) {
        }
        ServerEmployee serverEmployee = new ServerEmployee(port);
        serverEmployee.start();
        System.out.println("servernya ada di port " + serverEmployee.getPort() + " gaes");

        BufferedReader sysin = new BufferedReader( new InputStreamReader( System.in ) );
        while ( true ) {
            String in = sysin.readLine();
            serverEmployee.broadcast( in );
            if( in.equals( "exit" ) ) {
                serverEmployee.stop(1000);
                break;
            }
        }
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        System.out.println("ada yg masuk gaes kayanya!");
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        System.out.println("yah dia keluar!");
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        Employee employee = new Employee();
        try {
            employee.addData(message);
            for (String data : employee.getDatas()){
                broadcast(data);
                System.out.println(data);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {
        ex.printStackTrace();
        if( conn != null ) {
            // some errors like port binding failed may not be assignable to a specific websocket
        }
    }

    @Override
    public void onStart() {
        System.out.println("servernya nyala gaes!");
    }
}

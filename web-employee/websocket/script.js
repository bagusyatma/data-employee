const connection = new WebSocket("ws://localhost:7070");
var dataEmployee = document.getElementById("dataEmployee");
var btnSubmit = document.getElementById("btnSubmit");

connection.onopen = function () {
  console.log("connect to server");
  doSend(
    JSON.stringify({
      type: "trigger"
    })
  )
};

const doSend = (message) => {
  connection.send(message)
}

connection.onmessage = function (e) {
  // const obj = new Object(e.data).toString();
  var json = JSON.parse(e.data);

  if (json.jenis_kelamin == "Pria") {
    dataEmployee.innerHTML += `<div class="card mb-4 shadow-sm">
                                <div class="card-header">
                                    <h4 class="my-0 font-weight-normal">` + json.nama + `</h4>
                                </div>
                                <div class="card-body">
                                    <img src="image/pria.png" class="rounded-circle" width="150">
                                    <h1 class="card-title pricing-card-title mt-3">` + json.nama + `</small></h1>
                                    <ul class="list-unstyled mt-2 mb-1">
                                        <li>
                                            <h5>` + json.tempat + `, ` + json.tanggal_lahir + `</h5>
                                        </li>
                                        <li>
                                            <h5>` + json.jenis_kelamin + `</h5>
                                        </li>
                                    </ul>
                                </div>
                            </div>`
  } else if (json.jenis_kelamin == "Wanita") {
    dataEmployee.innerHTML += `<div class="card mb-4 shadow-sm">
                                <div class="card-header">
                                    <h4 class="my-0 font-weight-normal">` + json.nama + `</h4>
                                </div>
                                <div class="card-body">
                                    <img src="image/wanita.png" class="rounded-circle" width="150">
                                    <h1 class="card-title pricing-card-title mt-3">` + json.nama + `</small></h1>
                                    <ul class="list-unstyled mt-2 mb-1">
                                        <li>
                                            <h5>` + json.tempat + `, ` + json.tanggal_lahir + `</h5>
                                        </li>
                                        <li>
                                            <h5>` + json.jenis_kelamin + `</h5>
                                        </li>
                                    </ul>
                                </div>
                            </div>`
  }
};

btnSubmit.onclick = function () {
  var nama = document.getElementById("nama").value
  var tempat_lahir = document.getElementById("tempat-lahir").value
  var tanggal_lahir = document.getElementById("tanggal-lahir").value
  var jenis_kelamin = document.getElementById("jenis-kelamin").value

  if (nama == "") {
    nama.classList.add("is-invalid");
  } else if (tempat_lahir == "") {
    tempat_lahir.classList.add("is-invalid")
  } else if (tanggal_lahir == "") {
    tanggal_lahir.classList.add("is-invalid");
  } else if (jenis_kelamin == "") {
    jenis_kelamin.classList.add("is-invalid");
  }

  doSend(
    JSON.stringify({
      type: "data",
      nama: nama,
      tempat_lahir: tempat_lahir,
      tanggal_lahir: tanggal_lahir,
      jenis_kelamin: jenis_kelamin
    })
  )

  window.location.href = "/index.html"
}
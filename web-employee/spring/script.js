function getItems() {
    fetch(`http://localhost:8080/employees`)
        .then(response => response.json())
        .then(data => renderItems(data));
};

function getItemById(id) {
    fetch(`http://localhost:8080/employee/${id}`)
        .then(response => response.json())
        .then(datas => renderItemById(datas))
        .catch(console.error);
}

function getItemForUpdate(id) {
    fetch(`http://localhost:8080/employee/${id}`)
        .then(response => response.json())
        .then(datas => editData(datas))
        .catch(console.error);
}

function renderItems(results) {
    // console.log(results);
    var dataEmployee = document.getElementById("dataEmployee")
    results.forEach((result) => {
        // console.log(result);
        dataEmployee.innerHTML += `<div class="card mb-4 shadow-sm">
                                    <div class="card-header">
                                        <h4 class="my-0 font-weight-normal float-right">
                                            <button class="btn btn-primary btn-sm" data-toggle="modal"
                                                data-target="#detailModal" id = "` + result.id + `" onclick="getItemById(id)"><i class="fas fa-eye"></i></button>
                                                <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#updateModal" onclick="getItemForUpdate(${result.id})"><i class="fas fa-marker"></i></button>
                                                <button class="btn btn-danger btn-sm" onclick="deleteData(${result.id})"><i class="fas fa-trash"></i></button>
                                        </h4>
                                    </div>
                                    <div class="card-body">
                                        <img src="image/${result.jenis_kelamin}.png" class="rounded-circle" width="150">
                                        <h1 class="card-title pricing-card-title mt-3">${result.nama}</h1>
                                        <h5>${result.tempat_lahir}, ${result.tanggal_lahir}</h5>
                                    </div>
                                </div>`

    });
}

function renderItemById(data) {
    var modalDetail = document.getElementById("detailModal")
    modalDetail.innerHTML = `<div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="${data.id}ModalLabel">${data.nama}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="card m-auto text-left" style="width: 22rem;">
                                            <img src="image/${data.jenis_kelamin}.png" class="card-img-top">
                                            <div class="card-body">
                                                <table class="table table-borderless">
                                                    <tr>
                                                        <th>NIK</th>
                                                        <td>${data.nik}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Nama</th>
                                                        <td>${data.nama}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Tempat, tgl lahir</th>
                                                        <td>${data.tempat_lahir}, ${data.tanggal_lahir}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Jenis Kelamin</th>
                                                        <td>${data.jenis_kelamin}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Alamat</th>
                                                        <td>${data.alamat}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>RT/RW</th>
                                                        <td>${data.rt}/${data.rw}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Kelurahan</th>
                                                        <td>${data.kelurahan}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Kecamatan</th>
                                                        <td>${data.kecamatan}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Agama</th>
                                                        <td>${data.agama}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Pekerjaan</th>
                                                        <td>${data.pekerjaan}</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>`
}

function deleteData(id) {
    var data = confirm("data akan dihapus? ")
    if (data) {
        fetch(`http://localhost:8080/delete/${id}`, {
                method: 'DELETE'
            })
            .then(response => location.reload())
            .catch(console.error)
    }
}

function editData(data) {
    var updateModal = document.getElementById("updateModal")
    updateModal.innerHTML = `<div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Edit Data Employee</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <input type="hidden" id="id" value="${data.id}">
                                        <div class="form-group">
                                            <label for="edit-nik">NIK</label>
                                            <input type="number" class="form-control" id="edit-nik" value="${data.nik}" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="edit-nama">Nama</label>
                                            <input type="text" class="form-control" id="edit-nama" value="${data.nama}">
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col">
                                                    <label for="edit-tempat-lahir">Tempat Lahir</label>
                                                    <input type="text" class="form-control" id="edit-tempat-lahir" value="${data.tempat_lahir}">
                                                </div>
                                                <div class="col">
                                                    <label for="edit-tanggal-lahir">Tanggal Lahir</label>
                                                    <input type="text" class="form-control" id="edit-tanggal-lahir" value="${data.tanggal_lahir}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="edit-jenis-kelamin">Jenis Kelamin</label>
                                            <select class="form-control" id="edit-jenis-kelamin">
                                                <option value="${data.jenis_kelamin}">${data.jenis_kelamin}</option>
                                                <option value="Pria">Pria</option>
                                                <option value="Wanita">Wanita</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="edit-alamat">Alamat</label>
                                            <input type="text" class="form-control" id="edit-alamat" value="${data.alamat}">
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col">
                                                    <label for="edit-rt">RT</label>
                                                    <input type="number" class="form-control" id="edit-rt" value="${data.rt}">
                                                </div>
                                                <div class="col">
                                                    <label for="edit-rw">RW</label>
                                                    <input type="number" class="form-control" id="edit-rw" value="${data.rw}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col">
                                                    <label for="edit-kelurahan">Kelurahan</label>
                                                    <input type="text" class="form-control" id="edit-kelurahan" value="${data.kelurahan}">
                                                </div>
                                                <div class="col">
                                                    <label for="edit-kecamatan">Kecamatan</label>
                                                    <input type="text" class="form-control" id="edit-kecamatan" value="${data.kecamatan}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="edit-agama">Agama</label>
                                            <select class="form-control" id="edit-agama">
                                                <option value="${data.agama}">${data.agama}</option>
                                                <option value="Islam">Islam</option>
                                                <option value="Protestan">Protestan</option>
                                                <option value="Katolik">Katolik</option>
                                                <option value="Hindu">Hindu</option>
                                                <option value="Buddha">Buddha</option>
                                                <option value="Khonghucu">Khonghucu</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="edit-pekerjaan">Pekerjaan</label>
                                            <input type="text" class="form-control" id="edit-pekerjaan" value="${data.pekerjaan}">
                                        </div>
                                        <br>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary" id="submit" onclick="updateData()">Submit</button>
                                        </div>
                                    </div>
                                </div>`
}

function updateData() {
    var id = document.getElementById("id").value
    var nik = document.getElementById("edit-nik").value
    var nama = document.getElementById("edit-nama").value
    var tempat_lahir = document.getElementById("edit-tempat-lahir").value
    var tanggal_lahir = document.getElementById("edit-tanggal-lahir").value
    var jenis_kelamin = document.getElementById("edit-jenis-kelamin").value
    var alamat = document.getElementById("edit-alamat").value
    var rt = document.getElementById("edit-rt").value
    var rw = document.getElementById("edit-rw").value
    var kelurahan = document.getElementById("edit-kelurahan").value
    var kecamatan = document.getElementById("edit-kecamatan").value
    var agama = document.getElementById("edit-agama").value
    var pekerjaan = document.getElementById("edit-pekerjaan").value

    if (nama == "" || tempat_lahir == "" || tanggal_lahir == "" || jenis_kelamin == "" || alamat == "" || rt == "" || rw == "" || kelurahan == "" || kecamatan == "" || agama == "" || pekerjaan == "") {
        alert("data tidak boleh kosong")
    }else{
        var str_json = JSON.stringify({
            id: id,
            nik: nik,
            nama: nama,
            tempat_lahir: tempat_lahir,
            tanggal_lahir: tanggal_lahir,
            jenis_kelamin: jenis_kelamin,
            alamat: alamat,
            rt: rt,
            rw: rw,
            kelurahan: kelurahan,
            kecamatan: kecamatan,
            agama: agama,
            pekerjaan: pekerjaan
        })

        fetch('http://localhost:8080/update', {
                method: 'PUT', // or 'PUT'
                headers: {
                    'Content-Type': 'application/json',
                },
                body: str_json,
            })
            .then(response => response.json())
            .then(data => {
                console.log('Success:', data);
                window.location.href = "/spring/index.html"
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    }
}

getItems();
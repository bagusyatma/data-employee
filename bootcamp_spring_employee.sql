-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 01, 2020 at 06:12 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bootcamp_spring_employee`
--

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `agama` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `jenis_kelamin` varchar(255) DEFAULT NULL,
  `kecamatan` varchar(255) DEFAULT NULL,
  `kelurahan` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `nik` int(11) NOT NULL,
  `pekerjaan` varchar(255) DEFAULT NULL,
  `rt` int(11) NOT NULL,
  `rw` int(11) NOT NULL,
  `tanggal_lahir` varchar(255) DEFAULT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `agama`, `alamat`, `jenis_kelamin`, `kecamatan`, `kelurahan`, `nama`, `nik`, `pekerjaan`, `rt`, `rw`, `tanggal_lahir`, `tempat_lahir`) VALUES
(1, 'islam', 'jalan kenangan', 'Pria', 'kenangan', 'kenang', 'Marshall', 12345678, 'heckel', 1, 1, '01-01-2001', 'Jakarta'),
(2, 'islam', 'jalan bandung tenggara', 'Wanita', 'bumi', 'suka', 'Marlyn', 87654321, 'perawat', 1, 1, '02-02-2002', 'Bandung'),
(3, 'islam', 'jalan maluku utara', 'Pria', 'lalu', 'masa', 'Jeremy', 12341234, 'pelajar', 1, 1, '03-03-2003', 'Wonogiri'),
(4, 'Islam', 'kebon nanas', 'Wanita', 'nanas', 'kebon', 'Bunga', 43214321, 'model', 12, 1, '24-07-2020', 'Ciamis'),
(5, 'Islam', 'kapuk pinggir bandara', 'Pria', 'kapuk kali', 'audah', 'Adit Tolo', 89898989, 'heckel propesional', 34, 12, '31-07-2020', 'Jepara');

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(6);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
